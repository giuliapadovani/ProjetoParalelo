$(document).ready(function(){
      $(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > $("#home").height()) { // check if user scrolled more than 50 from top of the browser window
          $(".menu").css("background", "rgb(23, 172, 140)"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
        } else {
          $(".menu").css("background", "rgba(12, 39, 40, 0.31)"); // if not, change it back to transparent
        }
      });
    });
